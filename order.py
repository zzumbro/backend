from pydantic import BaseModel


class Order(BaseModel):
    number: str
    user: str
