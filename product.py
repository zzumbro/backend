from enum import Enum
from typing import List, Optional

from pydantic import BaseModel


class Unit(str, Enum):
    pound = "lb"


class ProductDetail(BaseModel):
    price: float
    unit: str
    description: Optional[str] = None


class Product(BaseModel):
    name: str
    description: str
    type: str
    details: List[ProductDetail]


def products():
    return [
        Product(
            name="Coffee Beans",
            description="Whole coffee beans",
            type="whole sale",
            details=[ProductDetail(price=13.70, unit=Unit.pound)],
        ),
        Product(
            name="Granola",
            description="All Natural Granola",
            type="whole sale",
            details=[ProductDetail(price=13.70, unit=Unit.pound)],
        ),
    ]
